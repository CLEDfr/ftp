package test3;

public abstract class Command implements Runnable{

	private PI pi;
	private String cmdPrefix;
	private String[] args;
	private String ret;
	
	public void setParams(PI pi, String cmdPrefix, String... args) {
		this.pi = pi;
		this.cmdPrefix = cmdPrefix;
		this.args = args==null?new String[0]:args;
		ret = null;
	}
	
	public PI getPi() {
		return pi;
	}
	
	public String getPrefix() {
		return cmdPrefix;
	}
	
	public String[] getArgs() {
		return args;
	}
	
	protected void setReturn(String s) {
		this.ret = s;
	}
	
	public String getReturn() {
		return ret;
	}
	
}
