package test3;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public enum Commands {

	USER(new Command() {
		@Override
		public void run() {
			if (getArgs().length == 0)
				return;
			getPi().getWriter().print(getPrefix() + " " + getArgs()[0] + "\r\n");
			getPi().getWriter().flush();
			try {
				setReturn(getPi().getReader().readLine());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}), PASS(new Command() {
		@Override
		public void run() {
			if (getArgs().length == 0)
				return;
			getPi().getWriter().print(getPrefix() + " " + getArgs()[0] + "\r\n");
			getPi().getWriter().flush();
			try {
				setReturn(getPi().getReader().readLine());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}), PWD(new Command() {
		@Override
		public void run() {
			getPi().getWriter().print(getPrefix() + "\r\n");
			getPi().getWriter().flush();
			try {
				setReturn(getPi().getReader().readLine());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}), CWD(new Command() {
		@Override
		public void run() {
			if (getArgs().length == 0)
				return;
			getPi().getWriter().print(getPrefix() + " " + getArgs()[0] + "\r\n");
			getPi().getWriter().flush();
			try {
				setReturn(getPi().getReader().readLine());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}), CDUP(new Command() {
		@Override
		public void run() {
			getPi().getWriter().print(getPrefix() + "\r\n");
			getPi().getWriter().flush();
			try {
				setReturn(getPi().getReader().readLine());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}), QUIT(new Command() {
		@Override
		public void run() {
			getPi().getWriter().print(getPrefix() + "\r\n");
			getPi().getWriter().flush();
			try {
				setReturn(getPi().getReader().readLine());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}),

	TYPE(new Command() {
		@Override
		public void run() {
			if (getArgs().length == 0)
				return;
			getPi().getWriter().print(getPrefix() + " " + getArgs()[0] + "\r\n");
			getPi().getWriter().flush();
			try {
				setReturn(getPi().getReader().readLine());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}), STRU(new Command() {
		@Override
		public void run() {
			if (getArgs().length == 0)
				return;
			getPi().getWriter().print(getPrefix() + " " + getArgs()[0] + "\r\n");
			getPi().getWriter().flush();
			try {
				setReturn(getPi().getReader().readLine());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}), MODE(new Command() {
		@Override
		public void run() {
			if (getArgs().length == 0)
				return;
			getPi().getWriter().print(getPrefix() + " " + getArgs()[0] + "\r\n");
			getPi().getWriter().flush();
			try {
				setReturn(getPi().getReader().readLine());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}), PORT(new Command() {
		@Override
		public void run() {
			if (getArgs().length == 0)
				return;
			getPi().getWriter().print(getPrefix() + " " + getArgs()[0] + "\r\n");
			getPi().getWriter().flush();
			try {
				setReturn(getPi().getReader().readLine());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}), PASV(new Command() {
		@Override
		public void run() {
			getPi().getWriter().print(getPrefix() + "\r\n");
			getPi().getWriter().flush();
			try {
				setReturn(getPi().getReader().readLine());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}),

	RETR(new Command() {
		@Override
		public void run() {
			if (getArgs().length < 2)return;
			//System.out.println("a");
			File dest = new File(getArgs()[1]);
			dest.mkdirs();
			//System.out.println("b");
			String pasvret = Commands.PASV.run(getPi());
			Pattern p = Pattern.compile("\\((([0-9]+,?){4}),(([0-9]+,?){2})\\)");
			Matcher m = p.matcher(pasvret);
			//System.out.println("c");
			if (m.find()) {
				//System.out.println("d");
				String ip = m.group(1).replaceAll(",", ".");
				int port = 0;
				String[] sport = m.group(3).split(",");
				port = Integer.parseInt(sport[0]) * 256 + Integer.parseInt(sport[1]);
				System.out.println("ip: " + ip + " port: " + port);
				try {
					//System.out.println("e");
					DTP dtp = new DTP(InetAddress.getByName(ip), port);
					getPi().getWriter().print(getPrefix() + " " + getArgs()[0] + "\r\n");
					getPi().getWriter().flush();
					String r = "";
					//System.out.println("f");
					if(dest.exists())dest.delete();
					dest.createNewFile();
					FileWriter fw = new FileWriter(dest);
					//System.out.println("g");
					while(dtp.getReader().ready()) {
						char c = (char) dtp.getReader().read();
						//System.out.print(c);
						fw.write(c);
						r += c;
					}
					//System.out.println("=========================");
					fw.flush();
					fw.close();
					setReturn(r);
					return;
				} catch (Exception e) {
					e.printStackTrace();
				}
				//System.out.println("h");
			}
			//System.out.println("i");
			setReturn(pasvret);
		}
	}), STOR(new Command() {
		@Override
		public void run() {
			if (getArgs().length == 0)
				return;
			getPi().getWriter().print(getPrefix() + " " + getArgs()[0] + "\r\n");
			getPi().getWriter().flush();
			try {
				setReturn(getPi().getReader().readLine());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}), LIST(new Command() {
		@Override
		public void run() {
			if (getArgs().length == 0)
				return;
			getPi().getWriter().print(getPrefix() + " " + getArgs()[0] + "\r\n");
			getPi().getWriter().flush();
			try {
				setReturn(getPi().getReader().readLine());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}), NLST(new Command() {
		@Override
		public void run() {
			if (getArgs().length == 0)
				return;
			getPi().getWriter().print(getPrefix() + " " + getArgs()[0] + "\r\n");
			getPi().getWriter().flush();
			try {
				setReturn(getPi().getReader().readLine());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}), MKD(new Command() {
		@Override
		public void run() {
			if (getArgs().length == 0)
				return;
			getPi().getWriter().print(getPrefix() + " " + getArgs()[0] + "\r\n");
			getPi().getWriter().flush();
			try {
				setReturn(getPi().getReader().readLine());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}), RMD(new Command() {
		@Override
		public void run() {
			if (getArgs().length == 0)
				return;
			getPi().getWriter().print(getPrefix() + " " + getArgs()[0] + "\r\n");
			getPi().getWriter().flush();
			try {
				setReturn(getPi().getReader().readLine());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}), DELE(new Command() {
		@Override
		public void run() {
			if (getArgs().length == 0)
				return;
			getPi().getWriter().print(getPrefix() + " " + getArgs()[0] + "\r\n");
			getPi().getWriter().flush();
			try {
				setReturn(getPi().getReader().readLine());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}), HELP(new Command() {
		@Override
		public void run() {
			getPi().getWriter().print(getPrefix() + " " + (getArgs().length == 0 ? "" : getArgs()[0]) + "\r\n");
			getPi().getWriter().flush();
			try {
				setReturn(getPi().getReader().readLine());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}), NOOP(new Command() {
		@Override
		public void run() {
			if (getArgs().length == 0)
				return;
			getPi().getWriter().print(getPrefix() + " " + getArgs()[0] + "\r\n");
			getPi().getWriter().flush();
			try {
				setReturn(getPi().getReader().readLine());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}),

	;

	private Command cmd;

	private Commands(Command cmd) {
		this.cmd = cmd;
	}

	public String run(PI pi, String... args) {
		cmd.setParams(pi, this.name(), args);
		cmd.run();
		return cmd.getReturn();
	}

}
