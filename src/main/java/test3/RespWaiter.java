package test3;

import java.io.IOException;

public class RespWaiter extends Thread{

	ReadResp rr;
	String msg;
	
	public RespWaiter(ReadResp rr, String msg) {
		this.rr = rr;
		this.msg = msg;
	}
	
	@Override
	public void run() {
		System.out.println("suspending rr");
		rr.suspend();
		System.out.println("rr suspended");
		rr.pi.writer.print(msg);
		rr.pi.writer.flush();
		System.out.println("msg printed");
		try {
			System.out.println(rr.pi.reader.readLine());
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("resuming rr");
		rr.resume();
		System.out.println("rr resumed");
	}
	
}
