package test3;

import java.net.InetAddress;

public class PI extends ClientTCP {

	public PI(InetAddress ip, int port) {
		super(ip, port);
	}

	@Override
	public void run() {
		ReadResp rr = null;
		try {
			if (reader.readLine().startsWith("220")) {
				System.out.println("connected!");
				rr = new ReadResp(this);
				rr.start();
				Thread.sleep(500);
				//rr.waitMessageResp(Commands.PWD);
				rr.waitMessageResp(Commands.USER, "user");
				rr.waitMessageResp(Commands.PASS, "12345");
				/*rr.waitMessageResp(Commands.PWD);
				rr.waitMessageResp(Commands.CWD, "ftp");
				rr.waitMessageResp(Commands.PWD);
				rr.waitMessageResp(Commands.CDUP);
				rr.waitMessageResp(Commands.PWD);*/
				//rr.waitMessageResp(Commands.RETR, "pyftpdlib/*.py", "./a");
				final int tests = 15000;
				for(int i = 0; i < tests; i++) {
					System.out.println("----------------------------");
					rr.waitMessageResp(Commands.RETR, "ftpserv.py", "./a/b/c/retr.txt");
					System.out.println("test: "+(i+1)+"/"+tests);
				}
				System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
				rr.waitMessageResp(Commands.QUIT);
			}
			/*while(true) {
				System.out.print("");
			}*/
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(rr != null)  rr.stop();
	}

}
