package test3;

import java.io.IOException;
import java.util.Arrays;

public class ReadResp extends Thread{
	//a pour but de lire les msgs du serveur quand on attends pas de réponses
	PI pi;
	
	Commands cmd = null;
	String[] args = null;
	String resp = null;
	
	public ReadResp(PI pi) {
		this.pi = pi;
	}
	
	@Override
	public void run() {
		try {
			System.out.println("reading...");
			while(!pi.socket.isClosed()) {
				if(cmd != null) {
					System.out.println("validated!");
					/*pi.writer.print(msg);
					pi.writer.flush();
					msg = null;
					resp = pi.reader.readLine().replace("\\", "%");*/
					Commands c = cmd;
					String[] a = args;
					resp = c.run(pi, a);
					cmd = null;
					args = null;
				}
				if(pi.reader.ready()) {
					System.out.print("in > ");
					while(pi.reader.ready()) {
						char c = (char) pi.reader.read();
						System.out.print(c);
					}
					System.out.println();
				}
				//System.out.print(".");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String waitMessageResp(Commands cmd, String... args) {
		if(cmd == null)return null;
		this.cmd = cmd;
		this.args = args == null?new String[0]:args;
		System.out.println("cmd => \t"+cmd.name()+" "+Arrays.toString(this.args));
		while(resp == null) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		};
		String ret = resp;
		System.out.println("\t< "+ret);
		resp = null;
		return ret;
	}
	
}
