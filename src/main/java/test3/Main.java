package test3;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class Main {

	public static void main(String... args) throws NumberFormatException, UnknownHostException {
		new PI(args.length>0?InetAddress.getByName(args[0]):InetAddress.getByName("127.0.0.1"), args.length>1?Integer.parseInt(args[1]):9876);
	}
	
}
