package tests;

public class UnexpectedReturnException extends Exception{

	public UnexpectedReturnException(String ret) {
		super(ret);
	}
	
}
