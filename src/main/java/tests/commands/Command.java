package tests.commands;

import java.io.IOException;

import tests.PI;

public abstract class Command {

	private int[] goodCodes;
	
	public Command(int... goodCodes) {
		this.goodCodes = goodCodes;
	}
	
	public abstract String send(PI pi, String... args) throws IOException;

	public boolean succes(String s) {
		boolean res = false;
		if(goodCodes != null) {
			for(int i:goodCodes) {
				if(s.startsWith(""+i+" ")) {
					res = true;
					break;
				}
			}
		}
		return res;
	}
	
}
