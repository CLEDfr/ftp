package tests.commands;

import java.lang.reflect.Modifier;

import tests.PI;
import tests.UnexpectedReturnException;

public class CommandManager {

	private PI pi;
	
	public CommandManager(PI pi) {
		this.pi = pi;
	}
	
	public String executeCommand(Class<?> command, String... args) throws Exception {
		if(command == null || !Command.class.isAssignableFrom(command) || command.equals(Command.class) || Modifier.isAbstract(command.getModifiers()))return null;
		Command cmd = (Command) command.getConstructor().newInstance();
		String ret = cmd.send(pi, args);
		System.out.println(cmd.getClass().getSimpleName()+" > \n"+ret.replaceAll("\n", "\n\t"));
		if(!cmd.succes(ret)) {
			//traiter l'érreur
			throw new UnexpectedReturnException(ret);
		}
		return ret;
	}
	
}
