package tests.commands;

import java.io.IOException;

import tests.PI;

public class USER extends Command{

	public USER() {
		super(331);
	}
	
	@Override
	public String send(PI pi, String... args) throws IOException {
		pi.getWriter().print(this.getClass().getSimpleName()+" "+String.join(" ", args)+"\r\n");
		pi.getWriter().flush();
		String res = "";
		String s = null;
		do {
			s = pi.getReader().readLine();
			if(s == null)return null;
			res += s;
		}while(s.matches("^[0-9]+-.*$"));
		return res;
	}

}
