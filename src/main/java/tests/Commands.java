package tests;

import java.util.Arrays;
import java.util.List;

public enum Commands {

	USER(false, (List<Integer>)Arrays.asList(331)),
	PASS(false, (List<Integer>)Arrays.asList(230)),
	PWD(false, (List<Integer>)Arrays.asList(257)),
	CWD(false, (List<Integer>)Arrays.asList(250)),
	CDUP(false, (List<Integer>)Arrays.asList(250)),
	QUIT(false, (List<Integer>)Arrays.asList(221)),
	
	TYPE(false, (List<Integer>)Arrays.asList(200)),
	STRU(false, (List<Integer>)Arrays.asList(200)),
	MODE(false, (List<Integer>)Arrays.asList(200)),
	PORT(false, (List<Integer>)Arrays.asList(421)),
	
	RETR(false, (List<Integer>)Arrays.asList(150, 226)),
	STOR(false, (List<Integer>)Arrays.asList(150, 226)),
	LIST(false, (List<Integer>)Arrays.asList(150, 226)),
	NLST(false, (List<Integer>)Arrays.asList(150, 226)),
	MKD(false, (List<Integer>)Arrays.asList(257)),
	RMD(false, (List<Integer>)Arrays.asList(250)),
	DELE(false, (List<Integer>)Arrays.asList(250)),
	HELP(false, (List<Integer>)Arrays.asList(214)),
	NOOP(false, (List<Integer>)Arrays.asList(200)),
	;
	
	private final boolean dtp;
	private final List<Integer> expecteds;
	
	private Commands(boolean dtp, List<Integer> expecteds) {
		this.dtp = dtp;
		this.expecteds = expecteds;
	}
	
	public boolean needDTP() {
		return dtp;
	}
	
	public List<Integer> getExpecteds() {
		return expecteds;
	}
	
}
