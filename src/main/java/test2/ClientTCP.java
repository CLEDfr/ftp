package test2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class ClientTCP extends Thread{
	
	Socket socket;
	
	protected BufferedReader reader = null;
	protected PrintWriter writer = null;
	
	public ClientTCP(InetAddress ip, int port) {
		try {
			socket = new Socket(ip, port);
			System.out.println(socket.getRemoteSocketAddress());
			reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			writer = new PrintWriter(socket.getOutputStream());
			this.start();
		} catch (IOException e) {
			e.printStackTrace();
			end();
		}
	}
	
	public void end() {
		System.out.println("CLOSE!");
		try {
			socket.close();
		} catch (Exception e) {
		}
	}
	
	public BufferedReader getReader() {
		return reader;
	}
	
	public PrintWriter getWriter() {
		return writer;
	}
	
}
