package test2;

import java.io.IOException;

public class ReadResp extends Thread{
	//a pour but de lire les msgs du serveur quand on attends pas de réponses
	PI pi;
	
	String msg = null;
	String resp = null;
	
	public ReadResp(PI pi) {
		this.pi = pi;
	}
	
	@Override
	public void run() {
		try {
			System.out.println("reading...");
			while(pi.socket.isConnected()) {
				if(msg != null) {
					pi.writer.print(msg);
					pi.writer.flush();
					msg = null;
					resp = pi.reader.readLine().replace("\\", "%");
				}
				if(pi.reader.ready()) {
					System.out.println("in > "+pi.reader.readLine());
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String waitMessageResp(String msg) {
		System.out.println("msg => \t"+msg);
		this.msg = msg+"\r\n";
		while(resp == null) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		};
		String ret = resp;
		System.out.println("\t< "+ret);
		resp = null;
		return ret;
	}
	
}
