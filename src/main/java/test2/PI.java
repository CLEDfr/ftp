package test2;

import java.net.InetAddress;

public class PI extends ClientTCP {

	public PI(InetAddress ip, int port) {
		super(ip, port);
	}

	@Override
	public void run() {
		DTP dtp = null;
		try {
			if (reader.readLine().startsWith("220")) {
				System.out.println("connected!");
				ReadResp rr = new ReadResp(this);
				rr.start();
				Thread.sleep(500);
				rr.waitMessageResp("PWD");
				rr.waitMessageResp("USER user");
				rr.waitMessageResp("PASS 12345");
				Thread.sleep(500);
				rr.waitMessageResp("PWD");
				rr.waitMessageResp("PWD");
				rr.waitMessageResp("PWD");
				Thread.sleep(500);
				rr.waitMessageResp("PWD");
				rr.waitMessageResp("PWD");
				rr.waitMessageResp("HELP");
				rr.waitMessageResp("PWD");
			}
			while(true) {
				System.out.print("");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
